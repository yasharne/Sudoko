/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author yashar
 */
public class GeneticSudoku {

    long startTime;

    int round;
    public static int mutationPer = 98;
    ArrayList<Member> members = new ArrayList<>();
    ArrayList<Member> nextGen = new ArrayList<>();

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Enter 4 OR 9: ");
        int size = s.nextInt();
        new GeneticSudoku(size);
    }

    public GeneticSudoku(int size) {
        int crossPercent = 60;
        int population = 2000;
        startTime = System.currentTimeMillis();
        createPopulation(population, size);
        geneticSolve(crossPercent, size, population);
//        System.out.println(fitness(ch2));

    }

    private void createPopulation(int population, int size) {
        round = 0;
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        numbers = initNumbers(numbers, size);
        for (int i = 0; i < population; i++) {
            Collections.shuffle(numbers);
            Integer[][] per = new Integer[size][size];
            for (int j = 0; j < size; j++) {
                for (int k = 0; k < size; k++) {
                    per[j][k] = numbers.get(j * size + k);
                }
            }
            Member m = new Member();
            m.arr = per;
            m.fitness = fitness(per);
            members.add(m);

        }
    }

    public void geneticSolve(int crossPercent, int size, int population) {
        Random r = new Random();
        while (round < 1000) {
            round++;
            for (int k = 0; k < population / 2; k++) {

                int father = 0;
                int mother = 0;
                do {
                    father = r.nextInt(population);
                    mother = r.nextInt(population);
                } while (members.get(father).fitness <= crossPercent && members.get(mother).fitness <= crossPercent);

                if (crossover(father, mother, size)) {
                    return;

                }
            }

            copyArrayList(nextGen, members);

        }
        members.clear();
        nextGen.clear();
        new GeneticSudoku(size);
    }

    private void printBoard(Integer[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                System.out.print(board[i][j] + "\t");
            }
            System.out.println("");
        }
    }

    public int fitness(Integer[][] per) {
        int conflicts = 0;
        for (int i = 0; i < per.length; i++) {
            for (int j = 0; j < per.length; j++) {
                conflicts += getConflicts(per, i, j, per[i][j]);
            }
        }
        double f = 0;
        if (per.length == 9) {
            f = (double) (648 - conflicts) / 648 * 100;
        }
        if (per.length == 4) {
            f = (double) (48 - conflicts) / 48 * 100;
        }

        return (int) f;
    }

    public boolean crossover(int arr1, int arr2,int size) {
        Integer[][] a = members.get(arr1).arr;
        Integer[][] b = members.get(arr2).arr;
        Integer[][] temp1 = new Integer[size][size];
        Integer[][] temp2 = new Integer[size][size];

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                temp1[i][j] = 0;
                temp2[i][j] = 0;
            }
        }

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                int t1 = getConflicts(temp1, i, j, a[i][j]);
                int t2 = getConflicts(temp1, i, j, b[i][j]);
                //  System.out.println(t1 + "   " + t2);
                if (t1 >= t2) {
                    temp1[i][j] = b[i][j];
                    temp2[i][j] = a[i][j];
                }
                if (t1 < t2) {
                    temp1[i][j] = a[i][j];
                    temp2[i][j] = b[i][j];
                }
            }
        }
        Member m1 = new Member();
        m1.arr = temp1;
        m1.fitness = fitness(temp1);
        Member m2 = new Member();
        m2.arr = temp1;
        m2.fitness = fitness(temp1);
        nextGen.add(m1);
        nextGen.add(m2);

        if (m1.fitness == 100) {
            System.out.println("=========Solution Found=========");
            System.out.println("Total Time in ms: " + (System.currentTimeMillis() - startTime));
            printBoard(m1.arr);
            return true;
        } else if (m2.fitness == 100) {
            System.out.println("=========Solution Found=========");
            System.out.println("Total Time in ms: " + (System.currentTimeMillis() - startTime));
            printBoard(m2.arr);
            return true;
        } else {

            if (m1.fitness > mutationPer) {
                mutation(arr1, size);
            }
            if (m2.fitness > mutationPer) {
                mutation(arr2, size);
            }
            return false;
        }
    }

    public int getConflicts(Integer[][] per, int i, int j, int num) {
        int con = 0;
        for (int k = 0; k < per.length; k++) {
            if (num == per[i][k] && j != k) {
                con++;
            }
            if (num == per[k][j] && i != k) {
                con++;
            }

        }

        int s2 = (j / (int) Math.sqrt(per.length)) * (int) Math.sqrt(per.length);
        int s1 = (i / (int) Math.sqrt(per.length)) * (int) Math.sqrt(per.length);
        for (int m = 0; m < (int) Math.sqrt(per.length); m++) {
            for (int n = 0; n < (int) Math.sqrt(per.length); n++) {
                if (num == per[m + s1][n + s2]) {
                    if (i != m + s1 && j != n + s2) {
                        con++;
                    }
                }
            }
        }
        return con;
    }

    public class Member {

        Integer[][] arr;
        int fitness;

    }

    public void mutation(int numArr, int size) {
        Integer[][] arr = members.get(numArr).arr;
        int[] repeats = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
        for (int i = 1; i < 10; i++) {
            for (int j = 0; j < size; j++) {
                for (int k = 0; k < size; k++) {
                    if (arr[j][k] == i) {
                        repeats[i - 1]++;
                    }

                }

            }
        }
        int g = 0, l = 0;
        for (int i = 0; i < repeats.length; i++) {
            if (repeats[i] > size) {
                g = i + 1;
            }
            if (repeats[i] < size) {
                l = i + 1;
            }
        }
        if (g != 0) {
            replace(numArr, g, l, size);
        }

    }

    public void replace(int numarr, int from, int to, int size) {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (members.get(numarr).arr[i][j] == from && getConflicts(members.get(numarr).arr, i, j, from) > 1) {
                    members.get(numarr).arr[i][j] = to;
                    return;
                }

            }

        }
    }

    private ArrayList<Integer> initNumbers(ArrayList<Integer> numbers, int size) {
        numbers.clear();
        for (int i = 1; i <= size; i++) {
            for (int j = 1; j <= size; j++) {
                numbers.add(i);
            }
        }
        return numbers;
    }

    private void copyArrayList(ArrayList<Member> src, ArrayList<Member> dst) {
        dst.clear();
        for (int i = 0; i < src.size(); i++) {
            dst.add(src.get(i));
        }
        src.clear();
    }

}
