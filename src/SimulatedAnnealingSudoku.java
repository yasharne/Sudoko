/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.*;

/**
 *
 * @author yashar
 */
public class SimulatedAnnealingSudoku {
    
    private long startTime;

    public SimulatedAnnealingSudoku(int[][] board) {
        
        startTime = System.currentTimeMillis();
        solve(board);
    }

    public ArrayList<Integer> initNumbers(ArrayList<Integer> numbers, int size) {
        numbers.clear();
        for (int i = 1; i <= size; i++) {
            numbers.add(i);
        }

        return numbers;
    }

    public void solve(int[][] board) {

        ArrayList<Integer> numbers = new ArrayList<>();

        for (int i = 0; i < board.length; i++) {
            int miniBoardSize = (int) Math.sqrt(board.length);
            int x = i % miniBoardSize;
            int y = i / miniBoardSize;
            numbers = initNumbers(numbers, board.length);

            for (int row = miniBoardSize * y; row < (miniBoardSize * y) + miniBoardSize; row++) {
                for (int col = miniBoardSize * x; col < (miniBoardSize * x) + miniBoardSize; col++) {
                    if (board[row][col] != 0) {
                        numbers.remove(new Integer(board[row][col]));
                    }
                }
            }
            Collections.shuffle(numbers);
            for (int row = miniBoardSize * y; row < (miniBoardSize * y) + miniBoardSize; row++) {
                for (int col = miniBoardSize * x; col < (miniBoardSize * x) + miniBoardSize; col++) {
                    if (board[row][col] == 0) {
                        board[row][col] = numbers.remove(0);
                    }
                }
            }
        }
        printBoard(board);

        recurseSolve(board, 0.8, 0);
    }

    public int numConflicts(int[][] board) {
        int num = 0;
        HashMap<Integer, Integer> numbers = new HashMap<>();

        //count thru rows
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if (numbers.get(board[i][j]) == null) {
                    numbers.put(board[i][j], 1);
                } else {
                    numbers.put(board[i][j], numbers.get(board[i][j]) + 1);
                }
            }

            for (int j = 1; j <= board.length; j++) {
                if (numbers.get(j) != null && numbers.get(j) > 1) {
                    num += numbers.get(j) - 1;
                }
                numbers.put(j, null);
            }
        }
        for (int col = 0; col < board.length; col++) {
            for (int row = 0; row < board.length; row++) {
                if (numbers.get(board[row][col]) == null) {
                    numbers.put(board[row][col], 1);
                } else {
                    numbers.put(board[row][col], numbers.get(board[row][col]) + 1);
                }
            }

            for (int j = 1; j <= board.length; j++) {
                if (numbers.get(j) != null && numbers.get(j) > 1) {
                    num += numbers.get(j) - 1;
                }
                numbers.put(j, null); 
            }
        }
        return num;
    }

    public int[][] recurseSolve(int[][] board, double temperature, int iteration) {
        int initConflicts = numConflicts(board);

        if (initConflicts == 0) {
            System.out.println("=========Solution found!=========");
            System.out.println("Total Time in ms: " + (System.currentTimeMillis() - startTime));
            printBoard(board);
//            return board;
            System.exit(0);
        }
        int square = (int) (Math.random() * board.length);
        int miniBoardSize = (int) Math.sqrt(board.length);
        int xOffset = (square / miniBoardSize) * (int) Math.sqrt(board.length);
        int yOffset = (square % miniBoardSize) * (int) Math.sqrt(board.length);

        int x1, y1, x2, y2;
        do {
            x1 = (int) (Math.random() * miniBoardSize);
            y1 = (int) (Math.random() * miniBoardSize);
            x2 = (int) (Math.random() * miniBoardSize);
            y2 = (int) (Math.random() * miniBoardSize);
        } while (x1 == x2 && y1 == y2);


        iteration++;

        int[][] boardCandidate = new int[board.length][board.length];
        multiArrayCopy(board, boardCandidate);
        boardCandidate[xOffset + x1][yOffset + y1] = board[xOffset + x2][yOffset + y2];
        boardCandidate[xOffset + x2][yOffset + y2] = board[xOffset + x1][yOffset + y1];

        int newConflicts = numConflicts(boardCandidate);

        if (newConflicts < initConflicts) {
            multiArrayCopy(boardCandidate, board);
        } else {
            double probability = Math.exp((initConflicts - newConflicts) / temperature);
            double random = Math.random();
            if (random <= probability) {
                multiArrayCopy(boardCandidate, board);
            }
        }
//        printBoard(board);

        if (iteration > 4450) {
            return board;
        }

        double nextTemperature = updateTemp(temperature);
        return recurseSolve(board, nextTemperature, iteration);
    }

    public void multiArrayCopy(int[][] source, int[][] destination) {
        for (int a = 0; a < source.length; a++) {
            System.arraycopy(source[a], 0, destination[a], 0, source[a].length);
        }
    }

    public double updateTemp(double temperature) {
        temperature *= .8; 
        return temperature;
    }

    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        System.out.print("Enter 4 OR 9: ");
        int size = s.nextInt();
        int[][] board = new int[size][size];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                board[i][j] = 0;
            }
        }
        
        printBoard(board);

        new SimulatedAnnealingSudoku(board);
    }

    //scale later
//    private static void printBoard(int[][] board) {
//        for (int row = 0; row < board.length; row++) {
//            System.out.println();
//            if (row == 0) {
//                System.out.println("\n -----------------------");
//            }
//            for (int col = 0; col < board.length; col++) {
//                if (board[row][col] != 0) {
//                    if (col == 0) {
//                        System.out.print("| ");
//                    }
//                    System.out.print(board[row][col] + " ");
//                    if (col == 2 | col == 5 | col == 8) {
//                        System.out.print("| ");
//                    }
//                } else {
//                    if (col == 0) {
//                        System.out.print("| ");
//                    }
//                    System.out.print("-" + " ");
//                    if (col == 2 | col == 5 | col == 8) {
//                        System.out.print("| ");
//                    }
//                }
//            }
//            if (row == 2 | row == 5 | row == 8) {
//                System.out.print("\n -----------------------");
//            }
//        }
//        System.out.println();
//    }
        private static void printBoard(int[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                System.out.print(board[i][j] + "\t");
            }
            System.out.println("");
        }
    }
}
