
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author yashar
 */
public class Sudoku1 {

    private static long startTime;

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Enter 4 OR 9: ");
        int size = s.nextInt();
        int[][] board = createBoard(size);
        System.out.println("\n");
        System.out.println("=========Initial State=========");
        System.out.println("\n");
        printBoard(board);
        startTime = System.currentTimeMillis();
        if (end(board)) {
            long stopTime = System.currentTimeMillis();
            System.out.println("\n");
            System.out.println("=========Solution Found=========");
            System.out.println("\n");
            printBoard(board);
            System.out.println("\n");
            System.out.println("Total time in ms: " + (stopTime - startTime));
            System.out.println("\n");
            System.exit(0);
        }
        solveSudoku(board, 0, 0);

    }

    private static void solveSudoku(int[][] board, int row, int col) {

        for (int i = 0; i < board.length; i++) {
            int tmp = board[row][col];
            board[row][col] = board[row][i];
            board[row][i] = tmp;
            if (end(board)) {
                long stopTime = System.currentTimeMillis();
                System.out.println("\n");
                System.out.println("=========Solution Found=========");
                System.out.println("\n");
                printBoard(board);
                System.out.println("\n");
                System.out.println("Total time in ms: " + (stopTime - startTime));
                System.out.println("\n");
                System.exit(0);
            }

            getNext(board, row, col);
        }

    }

    private static int[][] createBoard(int size) {
        int board[][] = new int[size][size];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                board[i][j] = size - j;
            }
        }
        return board;
    }

    private static void printBoard(int[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                System.out.print(board[i][j] + "\t");
            }
            System.out.println("");
        }
    }

    private static boolean end(int[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (!checkMainBorad(board, i, j) || !checkMiniBoard(board, i, j)) {
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean checkMainBorad(int[][] board, int row, int coloumn) {
        int num = board[row][coloumn];
        for (int r = 0; r < board.length; r++) {
            if (r != row) {
                if (board[r][coloumn] == num) {
//                System.out.println("found : " + num + " in row : " + r + "coloumn: " + coloumn);
                    return false;
                }
            }
        }

        for (int c = 0; c < board.length; c++) {
            if (c != coloumn) {
                if (board[row][c] == num) {
//                System.out.println("num : " + num);
                    return false;
                }
            }
        }

        return true;
    }

    private static boolean checkMiniBoard(int[][] board, int row, int coloumn) {

        int num = board[row][coloumn];
//        int size = board.length;
        int tmpRow = ((int) (row / Math.sqrt(board.length)) * (int) Math.sqrt(board.length));
        int tmpColoumn = ((int) (coloumn / Math.sqrt(board.length)) * (int) Math.sqrt(board.length));

        for (int r = 0; r < Math.sqrt(board.length); r++) {
            for (int c = 0; c < Math.sqrt(board.length); c++) {
                if (board[tmpRow + r][tmpColoumn + c] == num
                        && tmpRow + r != row
                        && tmpColoumn + c != coloumn) {
//                    System.out.println("in mini board");
                    return false;

                }
            }
        }

        return true;
    }

    private static void getNext(int[][] board, int row, int col) {
        int current = row * board.length + col + 1;
        if (current < Math.pow(board.length, 2)) {
            solveSudoku(board, current / board.length, current % board.length);
        }
    }

}
